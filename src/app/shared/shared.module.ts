import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MoneyFormatPipe } from './pipes/money-format.pipe';
import { HeaderComponent } from './components/header/header.component';


@NgModule({
  declarations: [
    MoneyFormatPipe,
    HeaderComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    MoneyFormatPipe,
    HeaderComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class SharedModule { }
