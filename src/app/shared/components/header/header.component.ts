import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';

import { AppService } from 'src/app/services/app.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {

  @Input() cart = true;
  private destroy$ = new Subject<boolean>();
  shoppingCartCount: number = 0;

  constructor(
    private appService: AppService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.appService.listenEvent$.pipe(
      takeUntil(this.destroy$)
    ).subscribe(() => {
      const shoppingCart = JSON.parse(localStorage.getItem('shopping'));
      this.shoppingCartCount = shoppingCart?.length || 0;
    });
  }

  goCart() {
    if (this.shoppingCartCount > 0) {
      this.router.navigate(['cart']);
    } else {
      // Alerta de que no tiene productos en el carrito
    }
  }

  goToHome() {
    this.router.navigate(['']);
  }

  ngOnDestroy(): void {
    this.destroy$.next(true);
    this.destroy$.complete();
  }

}
