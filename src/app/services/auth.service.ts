import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor( private _http: HttpClient) { }

  getEncryptKey(rut: string): Observable<any> {
    let serviceUrl = `${environment.apiPath}rut/${rut}`;
    return this._http.get(serviceUrl).pipe(
      map((data: any) => data.publicKey)
    );
  }

  login(body: any) {
    let serviceUrl = `${environment.apiPath}login/user`;
    return this._http.post(serviceUrl, body);
  }
}
