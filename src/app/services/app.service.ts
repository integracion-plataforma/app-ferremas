import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  private listenEvent = new BehaviorSubject<boolean>(false);
  listenEvent$ = this.listenEvent.asObservable();

  constructor() { }

  changeEvent() {
    this.listenEvent.next(true);
  }
}
