import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor( private _http: HttpClient) { }

  getProducts(): Observable<any> {
    let serviceUrl = `${environment.apiPath}productos`;
    return this._http.get(serviceUrl);
  }

  updateStock(products: any[]) {
    let serviceUrl = `${environment.apiPath}updateStock`;
    return this._http.put(serviceUrl, { products });
  }

  createProduct(product: any) {
    let serviceUrl = `${environment.apiPath}createProduct`;
    return this._http.post(serviceUrl, product);
  }
}
