import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';

import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TransactionService {

  constructor(
    private _http: HttpClient
  ) { }

  create(amount: number): Observable<any> {
    const serviceUrl = `${environment.apiPath}createTransaction`;
    return this._http.post(serviceUrl, { amount }).pipe(
      map((data: any) => data.result)
    );
  }

  confirm(token: string) {
    const serviceUrl = `${environment.apiPath}confirmTransaction`;
    return this._http.put(serviceUrl, { token }).pipe(
      map((data: any) => data.result)
    );
  }
}
