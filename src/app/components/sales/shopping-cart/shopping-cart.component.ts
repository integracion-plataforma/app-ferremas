import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { take } from 'rxjs';

import { AppService } from 'src/app/services/app.service';
import { TransactionService } from 'src/app/services/transaction.service';


@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css']
})
export class ShoppingCartComponent implements OnInit {

  cartProduct: any[] = [];
  totalCart = 0;

  constructor(
    private transactionService: TransactionService,
    private appService: AppService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getCartProduct();
  }

  getCartProduct() {
    const shopping = JSON.parse(localStorage.getItem('shopping'));
    if (shopping) {
      this.cartProduct = shopping;
      this.totalCart = this.cartProduct.reduce((acc, product) => acc + (product.item.precio * product.quantity), 0);
    }
  }

  redireccionarAPago() {
    if (this.cartProduct?.length) {
      this.transactionService.create(this.totalCart).pipe(
        take(1)
      ).subscribe(data => {
        const form = document.createElement('form');
        form.method = 'post';
        form.action = data.url;

        const input = document.createElement('input');
        input.type = 'hidden';
        input.name = 'token_ws';
        input.value = data.token;

        form.appendChild(input);
        document.body.appendChild(form);
        form.submit();
      });
    }
  }

  deleteProduct(idProduct: string) {
    this.cartProduct = this.cartProduct.filter(product => product.item.codigo !== idProduct);
    this.totalCart = this.cartProduct.reduce((acc, product) => acc + (product.item.precio * product.quantity), 0);
    localStorage.setItem('shopping', JSON.stringify(this.cartProduct));
    this.appService.changeEvent();
  }

  goToHome() {
    this.router.navigate(['']);
  }

}
