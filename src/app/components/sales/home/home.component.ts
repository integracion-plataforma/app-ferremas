import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { AppService } from 'src/app/services/app.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  products: any[] = [];
  forms: FormGroup;

  addedProduct: any[] = [];

  constructor(
    private activatedRoute: ActivatedRoute,
    private fb: FormBuilder,
    private appService: AppService
  ) {
    this.forms = this.fb.group({
      items: this.fb.array([])
    });
  }

  ngOnInit(): void {
    const productsStore = JSON.parse(localStorage.getItem('shopping'));
    if (productsStore) {
      this.addedProduct = productsStore;
    }
    this.products = this.activatedRoute.snapshot.data['rslv'].productos;
    console.log('products: ', this.products);
    this.products.forEach(item => {
      this.itemsFormArray.push(this.createItemFormGroup(item));
    });
  }

  get itemsFormArray(): FormArray {
    return this.forms.get('items') as FormArray;
  }

  private createItemFormGroup(item: any): FormGroup {
    return this.fb.group({
      quantity: [0, [Validators.required, Validators.min(1), Validators.max(item.stock)]]
    });
  }

  getFormGroup(index: number): FormGroup {
    return this.itemsFormArray.at(index) as FormGroup;
  }

  increment(index: number) {
    const control = this.itemsFormArray.at(index) as FormGroup;
    const value = control.get('quantity')?.value + 1;
    if (value <= this.products[index].stock) {
      control.get('quantity')?.setValue(value);
    }
  }

  decrement(index: number) {
    const control = this.itemsFormArray.at(index) as FormGroup;
    const value = control.get('quantity')?.value - 1;
    if (value >= 0) {
      control.get('quantity')?.setValue(value);
    }
  }

  addItem(index: number) {
    const control = this.itemsFormArray.at(index) as FormGroup;
    if (control.valid) {
      const getIndexProduct = this.addedProduct.findIndex(product => product.item.codigo === this.products[index].codigo);

      if (getIndexProduct >= 0) {
        this.addedProduct[getIndexProduct].quantity += control.get('quantity')?.value;
      } else {
        this.addedProduct.push({ item: this.products[index], quantity: control.get('quantity')?.value });
      }
      control.get('quantity').reset(0);
      localStorage.setItem('shopping', JSON.stringify(this.addedProduct));
      this.appService.changeEvent();
    }
    console.log('CARRITO: ', this.addedProduct);
  }

}
