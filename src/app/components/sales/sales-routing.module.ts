import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { SalesResolver } from './resolver/sales.resolver';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { ConfirmTransactionComponent } from './confirm-transaction/confirm-transaction.component';
import { ConfirmTransactionResolver } from './resolver/confirm-transaction.resolver';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    resolve: { rslv: SalesResolver }
  },
  {
    path: 'cart',
    component: ShoppingCartComponent
  },
  {
    path: 'confirm-transaction',
    component: ConfirmTransactionComponent,
    resolve: { rslv: ConfirmTransactionResolver }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SalesRoutingModule { }
