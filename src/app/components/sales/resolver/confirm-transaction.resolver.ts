import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, finalize, forkJoin, map, of } from 'rxjs';

import { NgxSpinnerService } from 'ngx-spinner';

import { TransactionService } from 'src/app/services/transaction.service';
import { AppService } from 'src/app/services/app.service';
import { ProductService } from 'src/app/services/product.service';


@Injectable({
  providedIn: 'root'
})
export class ConfirmTransactionResolver implements Resolve<boolean> {

  constructor(
    private transactionService: TransactionService,
    private productService: ProductService,
    private appService: AppService,
    private spinner: NgxSpinnerService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> {
    this.spinner.show();
    const token = route.queryParamMap.get('token_ws') || route.queryParamMap.get('TBK_TOKEN');

    const products = JSON.parse(localStorage.getItem('shopping'));

    return forkJoin([
      this.transactionService.confirm(token),
      this.productService.updateStock(products)
    ]).pipe(
      map(([data]) => {
        const resultTransaction = data.confirm;
        if (resultTransaction) {
          localStorage.removeItem('shopping');
          this.appService.changeEvent();
        }
        return resultTransaction;
      }),
      finalize(() => this.spinner.hide())
    );
  }
}
