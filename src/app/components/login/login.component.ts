import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import JSEncrypt from 'jsencrypt';
import { switchMap, take } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  errorUser = false;
  errorPassword = false;

  loading = false;
  errorLogin = false;

  username: string = '';
  pass: string = '';

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  login() {
    if (!this.loading && this.username.length >= 7 && this.pass.length >= 4) {
      this.loading = true;
      console.log({ user: this.username, pwd: this.pass });

      this.authService.getEncryptKey(this.username).pipe(
        take(1),
        switchMap(publicKey => {
          const jsencrypt: JSEncrypt = new JSEncrypt();
          jsencrypt.setKey(publicKey);
          const passEncrypted = jsencrypt.encrypt(this.pass);
          const body = {
            rut: this.username,
            password: passEncrypted
          };
          return this.authService.login(body);
        })
      ).subscribe((response: any) => {
        if (response.code === '000') {
          this.errorLogin = false;
          this.router.navigate(['product']);
        } else {
          this.errorLogin = true;
        }
        this.loading = false;
      }, error => {
        this.loading = false;
        this.errorLogin = false;
      });
    }
  }

}
