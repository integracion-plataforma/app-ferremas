import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, finalize, of } from 'rxjs';

import { NgxSpinnerService } from 'ngx-spinner';

import { ProductService } from 'src/app/services/product.service';

@Injectable({
  providedIn: 'root'
})
export class ProductResolver implements Resolve<boolean> {

  constructor(
    private _productService: ProductService,
    private spinner: NgxSpinnerService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
    this.spinner.show();
    return this._productService.getProducts().pipe(
      finalize(() => this.spinner.hide())
    );
  }
}
