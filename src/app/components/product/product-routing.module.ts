import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProductListComponent } from './product-list/product-list.component';
import { ProductResolver } from './resolver/product.resolver';
import { ProductFormComponent } from './product-form/product-form.component';


const routes: Routes = [
  {
    path: '',
    component: ProductListComponent,
    resolve: { rslv: ProductResolver }
  },
  {
    path: ':type/form',
    component: ProductFormComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductRoutingModule { }
