import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { finalize, take } from 'rxjs';

import { NgxSpinnerService } from 'ngx-spinner';

import { ProductService } from 'src/app/services/product.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.css']
})
export class ProductFormComponent implements OnInit {

  form: FormGroup;

  constructor(
    private fb: FormBuilder,
    private productService: ProductService,
    private spinner: NgxSpinnerService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.form = this.fb.group({
      codigo_producto: ['', [Validators.required]],
      nombre: ['', [Validators.required]],
      marca: ['', [Validators.required]],
      stock: [null, [Validators.required, Validators.min(1)]],
      precio: [null, [Validators.required, Validators.min(1)]]
    });
  }

  submitForm() {
    if (this.form.valid) {
      this.spinner.show()
      this.productService.createProduct(this.form.value).pipe(
        take(1),
        finalize(() => this.spinner.hide())
      ).subscribe((resp: any) => {
        this.form.reset();
        alert('Producto Creado');
      });
    }
  }

  goBack() {
    this.router.navigate(['product']);
  }

}
